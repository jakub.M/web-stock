import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import ProductsContainer from './components/product/ProductsContainer';
import ClientsContainer from './components/client/ClientsContainer';
import SuppliersContainer from './components/supplier/SuppliersContainer';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/products" component={ProductsContainer} />
      <Route exact path="/clients" component={ClientsContainer} />
      <Route exact path="/" component={SuppliersContainer} />
    </Switch>
  </BrowserRouter>
);

export default App;
