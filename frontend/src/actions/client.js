import axios from 'axios';
import { SET_CLIENTS, EDIT_CLIENT, FINISH_EDITING, ACTION_FAILURE, POSTING_SUCCESS, DELETE, ADD_NEW } from './actionTypes';
import { endpoint } from '../constants/settings';

const setClients = clients => {
  return {
    type: SET_CLIENTS,
    payload: clients
  };
};

const deletClientFromStore = client => ({
  type: DELETE,
  payload: client
});

export const actionFailure = err => {
  return {
    type: ACTION_FAILURE,
    payload: err
  };
};

const postingSuccess = () => {
  return {
    type: POSTING_SUCCESS
  };
};

export const addNew = () => ({
  type: ADD_NEW
});

export const finishEditing = () => {
  return {
    type: FINISH_EDITING
  };
};

export const editClient = id => {
  return {
    type: EDIT_CLIENT,
    payload: id
  };
};

export const submitClient = client => {
  return dispatch => {
    axios
      .put(`${endpoint}/clients`, client)
      .then(() => dispatch(postingSuccess()))
      .catch(err => dispatch(actionFailure(err)));
  };
};

export const fetchClients = () => {
  return dispatch => {
    axios
      .get(`${endpoint}/clients`)
      .then(res => dispatch(setClients(res.data)))
      .catch(err => console.log(err));
  };
};


export const deleteClient = client => (
  /* eslint-disable no-underscore-dangle */
  dispatch => {
    axios
      .delete(`${endpoint}/clients/${client._id}`)
      .then(() => dispatch(deletClientFromStore(client)))
      .catch(err => dispatch(actionFailure(err)));
  }
);
