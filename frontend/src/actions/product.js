import axios from 'axios';
import { FETCH_PRODUCTS, ACTION_FAILURE, EDIT_PRODUCT, FINISH_EDITING, ADD_NEW, DELETE, ACTION_STARTING } from './actionTypes';
import { endpoint } from '../constants/settings';

const setProducts = products => ({
  type: FETCH_PRODUCTS,
  payload: products
});

const actionStarting = () => ({
  type: ACTION_STARTING,
});

const actionFailure = err => ({
  type: ACTION_FAILURE,
  payload: err
});

const deleteProductFromState = id => ({
  type: DELETE,
  payload: id
});

export const editProduct = id => ({
  type: EDIT_PRODUCT,
  payload: id
});

export const closeEditProduct = () => ({
  type: FINISH_EDITING
});

export const addNewProduct = () => ({
  type: ADD_NEW
});

export const putProduct = product => (
  dispatch => {
    dispatch(actionStarting());
    axios
      .put(`${endpoint}/products`, product)
      .then(() => dispatch(closeEditProduct()))
      .catch(err => dispatch(actionFailure(err)));
  }
);

export const postProduct = product => (
  dispatch => {
    dispatch(actionStarting());
    axios
      .post(`${endpoint}/products`, product)
      .then(() => dispatch(closeEditProduct()))
      .catch(err => dispatch(actionFailure(err)));
  }
);

export const fetchProducts = () => (
  dispatch => {
    dispatch(actionStarting());
    axios
      .get(`${endpoint}/products`)
      .then(res => dispatch(setProducts(res.data)))
      .catch(err => dispatch(actionFailure(err)));
  }
);

export const submitProduct = product => (
  product.id ? putProduct(product) : postProduct(product)
);

export const deleteProduct = product => (
  dispatch => {
    dispatch(actionStarting());
    /* eslint-disable no-underscore-dangle */
    axios
      .delete(`${endpoint}/products/${product._id}`)
      .then(() => dispatch(deleteProductFromState(product)))
      .catch(err => dispatch(actionFailure(err)));
  }
);
