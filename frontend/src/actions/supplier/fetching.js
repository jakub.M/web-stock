import axios from 'axios';
import { SET, FAILURE } from './types';
import { endpoint } from '../../constants/settings';

const setSuppliers = suppliers => ({
  type: SET,
  payload: suppliers
});

const fetchingFailure = err => ({
  type: FAILURE,
  payload: err
});

export const fetchSuppliers = () => (
  dispatch => {
    axios
      .get(`${endpoint}/suppliers`)
      .then(res => dispatch(setSuppliers(res.data)))
      .catch(err => dispatch(fetchingFailure(err)));
  }
);
