export const SET = 'SET';
export const FAILURE = 'FAILURE';
export const DELETE = 'DELETE';
export const EDIT = 'EDIT';
export const CLOSE = 'CLOSE';
export const NEW = 'NEW';
export const SUCCESS = 'SUCCESS';
