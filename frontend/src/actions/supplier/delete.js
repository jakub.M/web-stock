import Axios from 'axios';
import { DELETE, FAILURE } from './types';
import { endpoint } from '../../constants/settings';

const deleteSupplierFromState = supplier => ({
  type: DELETE,
  payload: supplier
});

const deletingFailure = err => ({
  type: FAILURE,
  payload: err
});

export const deleteSupplier = supplier => (
  dispatch => (
    /* eslint-disable no-underscore-dangle */
    Axios
      .delete(`${endpoint}/suppliers/${supplier._id}`)
      .then(() => dispatch(deleteSupplierFromState(supplier)))
      .catch(err => dispatch(deletingFailure(err)))
  )
);
