import Axios from 'axios';
import { EDIT, CLOSE, NEW, SUCCESS, FAILURE } from './types';
import { endpoint } from '../../constants/settings';

export const editSupplier = id => ({
  type: EDIT,
  payload: id
});

export const closeEditingSupplier = () => ({
  type: CLOSE
});

export const newSupplier = () => ({
  type: NEW
});

const savingSuccess = () => ({
  type: SUCCESS
});

const savingFailure = err => ({
  type: FAILURE,
  payload: err
});

const postSupplier = supplier => (
  dispatch => (
    Axios
      .post(`${endpoint}/suppliers`, supplier)
      .then(() => dispatch(savingSuccess()))
      .catch(err => dispatch(savingFailure(err)))
  )
);

const putSupplier = supplier => (
  dispatch => (
    Axios
      .put(`${endpoint}/suppliers`, supplier)
      .then(() => dispatch(savingSuccess()))
      .catch(err => dispatch(savingFailure(err)))
  )
);

export const submitSupplier = supplier => (
  supplier.id ? putSupplier(supplier) : postSupplier(supplier)
);
