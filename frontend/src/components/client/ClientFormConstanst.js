export const ClientItems = [
  {
    name: 'name',
    placeholder: 'Nazwa'
  },
  {
    name: 'tax',
    placeholder: 'NIP'
  },
  {
    name: 'zip',
    placeholder: 'Kod Pocztowy'
  },
  {
    name: 'city',
    placeholder: 'Miato'
  },
  {
    name: 'address',
    placeholder: 'Adres'
  }
];
