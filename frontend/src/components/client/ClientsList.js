import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';

const ClientsList = ({ clients, editClient, deleteClient, addNew }) => {
  return (
    <div>
      <Table hover>
        <thead>
          <tr>
            <th>Nazwa</th>
            <th>Adres</th>
            <th>NIP</th>
            <th>Akcje</th>
          </tr>
        </thead>
        <tbody>
          {clients.map((client, index) => (
            <tr key={index}>
              <td>{client.name}</td>
              <td>
                <p>{`${client.zip} ${client.city}`}</p>
                <p>{client.address}</p>
              </td>
              <td>{client.tax}</td>
              <td>
                <button type="button" onClick={() => editClient(client.id)}>i</button>
                <button type="button" onClick={() => deleteClient(client)}>x</button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <button type="button" onClick={() => addNew()}>Dodaj</button>
    </div>
  );
};

ClientsList.propTypes = {
  clients: PropTypes.array.isRequired,
  editClient: PropTypes.func.isRequired,
  deleteClient: PropTypes.func.isRequired,
  addNew: PropTypes.func.isRequired,
};

export default ClientsList;
