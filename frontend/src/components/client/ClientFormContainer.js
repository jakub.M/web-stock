import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';

import { finishEditing, submitClient } from '../../actions/client';
import ClientForm from './ClientForm';


const ClientFormContainer = ({ finishEditing, submitClient, handleSubmit, initialValues }) => (
  <ClientForm
    close={finishEditing}
    handleSubmit={handleSubmit}
    initialValues={initialValues}
    onSubmit={submitClient}
  />
);

ClientFormContainer.propTypes = {
  finishEditing: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  submitClient: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    finishEditing,
    submitClient
  }, dispatch);
};

const mapStateToProps = state => {
  return {
    initialValues: state.clientReducer.client,
  };
};

const ClientFormContainerResult = reduxForm({ form: 'Client' })(ClientFormContainer);

export default connect(mapStateToProps, mapDispatchToProps)(ClientFormContainerResult);
