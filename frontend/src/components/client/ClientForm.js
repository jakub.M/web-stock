import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { ClientItems } from './ClientFormConstanst';

const ClientForm = ({ handleSubmit, close, onSubmit }) => (
  <form onSubmit={handleSubmit(onSubmit)}>
    {ClientItems.map((item, index) => (
      <Field
        key={index}
        name={item.name}
        component="input"
        type="text"
        placeholder={item.placeholder}
      />
    ))}
    <button type="submit">Zapisz</button>
    <button type="button" onClick={() => close()}>Anuluj</button>
  </form>
);

ClientForm.propTypes = {
  close: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default ClientForm;
