import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ClientFormContainer from './ClientFormContainer';
import ClientsListContainer from './ClientsListContainer';

const ClientsContainer = ({ isEditing }) => (
  isEditing ? <ClientFormContainer /> : <ClientsListContainer />
);

ClientsContainer.propTypes = {
  isEditing: PropTypes.bool.isRequired,
};

const mapStateToProps = state => {
  return {
    isEditing: state.clientReducer.isEditing
  };
};

export default connect(mapStateToProps)(ClientsContainer);
