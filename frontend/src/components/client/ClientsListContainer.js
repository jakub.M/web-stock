import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchClients, editClient, deleteClient, addNew } from '../../actions/client';
import ClientsList from './ClientsList';

class ClientsListContainer extends Component {
  static propTypes = {
    fetchClients: PropTypes.func.isRequired,
    clients: PropTypes.array.isRequired,
    editClient: PropTypes.func.isRequired,
    deleteClient: PropTypes.func.isRequired,
    addNew: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { fetchClients } = this.props;
    fetchClients();
  }

  render() {
    const { clients, editClient, deleteClient, addNew } = this.props;
    return (
      <ClientsList clients={clients} editClient={editClient} deleteClient={deleteClient} addNew={addNew} />
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchClients,
  editClient,
  deleteClient,
  addNew
}, dispatch);

const mapStateToProps = state => ({
  clients: state.clientReducer.clients,
});

export default connect(mapStateToProps, mapDispatchToProps)(ClientsListContainer);
