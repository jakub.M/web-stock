import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SupplierListContainer from './SupplierListContainer';
import SupplierFormContainer from './SupplierFormContainer';

const SuppliersContainer = ({ editing }) => (
  editing ? <SupplierFormContainer /> : <SupplierListContainer />
);

SuppliersContainer.propTypes = {
  editing: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  editing: state.supplierReducer.isEditing,
});

export default connect(mapStateToProps, null)(SuppliersContainer);
