import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchSuppliers } from '../../actions/supplier/fetching';
import { deleteSupplier } from '../../actions/supplier/delete';
import { editSupplier, newSupplier } from '../../actions/supplier/handlingForm';
import { SupplierList } from './SupplierList';

class SupplierListContainer extends Component {
  static propTypes = {
    fetchSuppliers: PropTypes.func.isRequired,
    suppliers: PropTypes.array.isRequired,
    deleteSupplier: PropTypes.func.isRequired,
    editSupplier: PropTypes.func.isRequired,
    newSupplier: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { fetchSuppliers } = this.props;
    fetchSuppliers();
  }

  render() {
    const { suppliers, deleteSupplier, editSupplier, newSupplier } = this.props;
    return (
      <SupplierList
        suppliers={suppliers}
        deleteSupplier={deleteSupplier}
        editSupplier={editSupplier}
        newSupplier={newSupplier}
      />
    );
  }
}

const mapStateToProps = state => ({
  suppliers: state.supplierReducer.suppliers,
});

const mapDispatchToProps = disppatch => bindActionCreators({
  fetchSuppliers,
  deleteSupplier,
  editSupplier,
  newSupplier
}, disppatch);

export default connect(mapStateToProps, mapDispatchToProps)(SupplierListContainer);
