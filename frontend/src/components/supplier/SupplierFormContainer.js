import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import SupplierForm from './SupplierForm';
import { closeEditingSupplier, submitSupplier } from '../../actions/supplier/handlingForm';

const SupplierFormContainer = ({ initialValues, handleSubmit, closeEditingSupplier, submitSupplier }) => (
  <SupplierForm
    initialValues={initialValues}
    close={closeEditingSupplier}
    handleSubmit={handleSubmit}
    onSubmit={submitSupplier}
  />
);

SupplierFormContainer.propTypes = {
  initialValues: PropTypes.object.isRequired,
  closeEditingSupplier: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitSupplier: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  initialValues: state.supplierReducer.supplier,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  closeEditingSupplier,
  submitSupplier
}, dispatch);

const SupplierFormContainerResult = reduxForm({ form: 'Supplier' })(SupplierFormContainer);

export default connect(mapStateToProps, mapDispatchToProps)(SupplierFormContainerResult);
