import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';

export const SupplierList = ({ suppliers, deleteSupplier, editSupplier, newSupplier }) => (
  <div>
    <Table hover>
      <thead>
        <tr>
          <th>Nazwa</th>
          <th>Adres</th>
          <th>NIP</th>
          <th>Akcje</th>
        </tr>
      </thead>
      <tbody>
        {suppliers.map((supplier, index) => (
          <tr key={index}>
            <td>{supplier.name}</td>
            <td>
              <p>{`${supplier.zip} ${supplier.city}`}</p>
              <p>{supplier.address}</p>
            </td>
            <td>{supplier.tax}</td>
            <td>
              <button type="button" onClick={() => editSupplier(supplier.id)}>i</button>
              <button type="button" onClick={() => deleteSupplier(supplier)}>x</button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
    <button type="button" onClick={() => newSupplier()}>Dodaj</button>
  </div>
);


SupplierList.propTypes = {
  suppliers: PropTypes.array.isRequired,
  deleteSupplier: PropTypes.func.isRequired,
  editSupplier: PropTypes.func.isRequired,
  newSupplier: PropTypes.func.isRequired,
};
