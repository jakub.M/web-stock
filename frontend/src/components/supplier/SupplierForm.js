import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { SupplierFormItems } from './SupplierFormItems';

const SupplierForm = ({ handleSubmit, onSubmit, close }) => (
  <form onSubmit={handleSubmit(onSubmit)}>
    {SupplierFormItems.map((item, index) => (
      <Field
        key={index}
        name={item.name}
        component="input"
        type="text"
        placeholder={item.placeholder}
      />
    ))}
    <button type="submit">Zapisz</button>
    <button type="button" onClick={() => close()}>Anuluj</button>
  </form>
);

SupplierForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
};

export default SupplierForm;
