import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import ProductForm from './ProductForm';
import { submitProduct, closeEditProduct } from '../../actions/product';

const ProductFormContainer = ({ initialValues, submitProduct, handleSubmit, closeEditProduct }) => (
  <ProductForm
    initialValues={initialValues}
    onSubmit={submitProduct}
    handleSubmit={handleSubmit}
    close={closeEditProduct}
  />
);

ProductFormContainer.propTypes = {
  initialValues: PropTypes.object.isRequired,
  submitProduct: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  closeEditProduct: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  initialValues: state.productReducer.product
});

const mapDispatchToProps = dispatch => bindActionCreators({
  submitProduct,
  closeEditProduct
}, dispatch);

const ProductFormContainerResult = reduxForm({ form: 'product' })(ProductFormContainer);

export default connect(mapStateToProps, mapDispatchToProps)(ProductFormContainerResult);
