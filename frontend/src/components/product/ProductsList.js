import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';

const ProductsList = ({ products, editProduct, addNewProduct, deleteProduct }) => (
  <div>
    <Table hover>
      <thead>
        <tr>
          <th>Nazwa</th>
          <th>Cena</th>
          <th>VAT</th>
          <th>action</th>
        </tr>
      </thead>
      <tbody>
        {products.map((product, index) => (
          <tr key={index}>
            <td>{product.name}</td>
            <td>{product.price}</td>
            <td>{product.tax}</td>
            <td>
              <button type="button" onClick={() => deleteProduct(product)}>x</button>
              <button type="button" onClick={() => editProduct(product.id)}>i</button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
    <button type="button" onClick={() => addNewProduct()}>Dodaj</button>
  </div>
);

ProductsList.propTypes = {
  products: PropTypes.array.isRequired,
  editProduct: PropTypes.func.isRequired,
  addNewProduct: PropTypes.func.isRequired,
  deleteProduct: PropTypes.func.isRequired,
};

export default ProductsList;
