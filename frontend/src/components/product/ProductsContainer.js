import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ProductFormContainer from './ProductFormContainer';
import ProductsListContainer from './ProductsListContainer';

const ProductsContainer = ({ isEditing }) => (
  isEditing ? <ProductFormContainer /> : <ProductsListContainer />
);

ProductsContainer.propTypes = {
  isEditing: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isEditing: state.productReducer.editing
});

export default connect(mapStateToProps)(ProductsContainer);
