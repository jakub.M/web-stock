import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { productItems } from './ProductFormConstant';

const ProductForm = ({ handleSubmit, onSubmit, close }) => (
  <form onSubmit={handleSubmit(onSubmit)}>
    {productItems.map((item) => (
      <Field
        key={item.name}
        name={item.name}
        component="input"
        placeholder={item.placeholder}
        type="text"
      />
    ))}
    <button type="submit">Zapisz</button>
    <button type="button" onClick={close}>Anuluj</button>
  </form>
);

ProductForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
};

export default ProductForm;
