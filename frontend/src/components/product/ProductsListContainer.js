import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchProducts, editProduct, addNewProduct, deleteProduct } from '../../actions/product';
import ProductsList from './ProductsList';

class ProductsListContainer extends Component {
  static propTypes = {
    fetchProducts: PropTypes.func.isRequired,
    editProduct: PropTypes.func.isRequired,
    products: PropTypes.array.isRequired,
    addNewProduct: PropTypes.func.isRequired,
    deleteProduct: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { fetchProducts } = this.props;
    fetchProducts();
  }

  render() {
    const { products, editProduct, addNewProduct, deleteProduct } = this.props;
    return (
      <ProductsList
        products={products}
        editProduct={editProduct}
        addNewProduct={addNewProduct}
        deleteProduct={deleteProduct}
      />
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchProducts,
  editProduct,
  addNewProduct,
  deleteProduct
}, dispatch);

const mapStateToProps = state => ({
  products: state.productReducer.products
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductsListContainer);
