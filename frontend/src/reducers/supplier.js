import { SET, FAILURE, DELETE, EDIT, CLOSE, NEW, SUCCESS } from '../actions/supplier/types';

const initialState = {
  suppliers: [],
  supplier: {},
  err: null,
  isEditing: false,
  success: false
};

const supplierReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET:
      return { ...state, suppliers: payload };
    case FAILURE:
      return { ...state, err: payload };
    case DELETE:
      return { ...state, suppliers: state.suppliers.filter(supplier => supplier !== payload) };
    case EDIT:
      return { ...state, isEditing: true, supplier: state.suppliers.find(supplier => supplier.id === payload) };
    case CLOSE:
      return { ...state, isEditing: false };
    case NEW:
      return { ...state, isEditing: true, supplier: {} };
    case SUCCESS:
      return { ...state, success: true, isEditing: false };
    default:
      return { ...state };
  }
};

export default supplierReducer;
