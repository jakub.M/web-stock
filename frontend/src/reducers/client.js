import { SET_CLIENTS, EDIT_CLIENT, FINISH_EDITING, POSTING_FAILURE, POSTING_SUCCESS, DELETE, ADD_NEW } from '../actions/actionTypes';

const initialState = {
  clients: [],
  client: {},
  isEditing: false,
  err: null,
  posttingSuccess: null
};

const clientReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_CLIENTS:
      return { ...state, clients: payload };
    case EDIT_CLIENT:
      return { ...state, isEditing: true, client: state.clients.find(client => client.id === payload) };
    case FINISH_EDITING:
      return { ...state, isEditing: false };
    case POSTING_FAILURE:
      return { ...state, err: payload };
    case POSTING_SUCCESS:
      return { ...state, posttingSuccess: true, isEditing: false, client: {} };
    case DELETE:
      return { ...state, clients: state.clients.filter(client => client !== payload) };
    case ADD_NEW:
      return { ...state, client: {}, isEditing: true };
    default:
      return state;
  }
};

export default clientReducer;
