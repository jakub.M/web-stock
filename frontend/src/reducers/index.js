import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import clientReducer from './client';
import productReducer from './product';
import supplierReducer from './supplier';

const reducers = combineReducers({
  clientReducer,
  productReducer,
  supplierReducer,
  form: reduxFormReducer
});

export default reducers;
