import { ACTION_FAILURE, FETCH_PRODUCTS, EDIT_PRODUCT, FINISH_EDITING, ADD_NEW, DELETE, ACTION_STARTING } from '../actions/actionTypes';

const initialState = {
  products: [],
  product: {},
  actionsSucces: false,
  err: null,
  editing: false
};

const productReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_PRODUCTS:
      return { ...state, products: payload, actionsSucces: true };
    case ACTION_FAILURE:
      return { ...state, err: payload, axtionSucces: false };
    case EDIT_PRODUCT:
      return { ...state, editing: true, product: state.products.find(product => product.id === payload) };
    case FINISH_EDITING:
      return { ...state, editing: false };
    case ADD_NEW:
      return { ...state, editing: true, product: {} };
    case DELETE:
      return { ...state, products: state.products.filter(product => product !== payload) };
    case ACTION_STARTING:
      return { ...state, actionsSucces: false };
    default:
      return state;
  }
};

export default productReducer;
