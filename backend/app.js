const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const morgan = require('morgan')

const clientRouter = require('./routers/client');
const productRouter = require('./routers/product');
const supplierRouter = require('./routers/supplier');

require('./db');

const app = new express();

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(cors())
app.use(morgan('dev'))

app.use('/clients', clientRouter)
app.use('/products', productRouter)
app.use('/suppliers', supplierRouter)

module.exports = app;