const express = require('express');
const router = express.Router();
const Client = require('../models/client')

router.get('/',async (req, res) => {
    await Client.find({}, (err, client)=>{
       if(err) return res.status(500).send(err)
       return res.status(200).send(client) 
    })
});

router.post('/', async (req, res) =>{
    const client = new Client(req.body);
    await client.save((err)=> {
        if(err) return res.status(400).send(err)
        return res.status(201).send(client)
    })
})
router.put('/', async(req, res) =>{
    await Client.findByIdAndUpdate(req.body._id, req.body, {new: true, runValidators: true}, (err, client) =>{
        if(err) return res.status(400).send(err);
        return res.status(200).send(client)
    })
})

router.delete('/:id', async(req, res) => {
    console.log(req.params.id)
    await Client.findByIdAndDelete(req.params.id, (err)=>{
        if(err) return res.status(400).send(err);
        return res.status(200).send("deleted successfuly");
    })
})

module.exports = router;
