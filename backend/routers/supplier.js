const express = require('express');
const router = express.Router();
const Supplier = require('../models/supplier')

router.get('/',async (req, res) => {
    await Supplier.find({}, (err, suppliers)=>{
       if(err) return res.status(500).send(err)
       return res.status(200).send(suppliers) 
    })
});

router.post('/', async (req, res) =>{
    const supplier = new Supplier(req.body);
    await supplier.save((err)=> {
        if(err) return res.status(400).send(err)
        return res.status(201).send(supplier)
    })
})
router.put('/', async(req, res) =>{
    await Supplier
    .findByIdAndUpdate(
      req.body._id, req.body,
      {new: true, runValidators: true},
      (err, supplier) => {
        if(err) return res.status(400).send(err);
        return res.status(200).send(supplier)
      }
    )
})

router.delete('/:id', async(req, res) => {
    await Supplier.findByIdAndDelete(req.params.id, (err)=>{
        if(err) return res.status(400).send(err);
        return res.status(200).send("deleted successfuly");
    })
})

module.exports = router;
