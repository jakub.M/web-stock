const express = require('express');
const router = express.Router();
const Product = require('../models/product')

router.get('/',async (req, res) => {
    await Product.find({}, (err, product)=>{
       if(err) return res.status(500).send(err)
       return res.status(200).send(product) 
    })
});

router.post('/', async (req, res) =>{
    const product = new Product(req.body);
    await product.save((err)=> {
        if(err) return res.status(400).send(err)
        return res.status(201).send(product)
    })
})
router.put('/', async(req, res) =>{
    await Product.findByIdAndUpdate(req.body._id, req.body, {new: true, runValidators: true}, (err, product) =>{
        if(err) return res.status(400).send(err);
        return res.status(200).send(product)
    })
})

router.delete('/:id', async(req, res) => {
    await Product.findByIdAndDelete(req.params.id, (err)=>{
        if(err) return res.status(400).send(err);
        return res.status(200).send("deleted successfuly");
    })
})

module.exports = router;
