const { Schema, model }  = require('mongoose');
const ObjectId = Schema.Types.ObjectId;

const productShema = new Schema({
  id:{
    type: ObjectId,
    auto: true,
    required:[true, "Reqired"],
  },
  name:{
    type: String,
    required: true
  },
  tax:{
    type: Number,
    required: true
  },
  price:{
    type: Number,
    required: true
  }
});

module.exports = model('Product', productShema);