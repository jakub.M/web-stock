const { Schema, model }  = require('mongoose');
const ObjectId = Schema.Types.ObjectId;

const clientSchema = new Schema({
  id:{
    type: ObjectId,
    auto: true,
    required:[true, "Reqired"],
  },
  name:{
    type: String,
    required: true
  },
  tax:{
    type: String,
    required: true
  },
  city:{
    type: String,
    required: true
  },
  zip:{
    type: String,
    required: true
  },
  address:{
    type: String,
    required: true
  },
});

module.exports = model('Client', clientSchema);