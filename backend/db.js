const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/webstock',{
  useNewUrlParser: true
});

const { connection } = mongoose;

connection.on( 'error', err => console.log(err));
connection.once( 'open', () => console.log('DB connected'));